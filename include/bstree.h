/*
 * bstree.h
 *
 *  Created on: 26 ott 2018
 *      Author: tbruni
 */

#ifndef BSTREE_H_
#define BSTREE_H_


struct bstree_node {
	struct bstree_node *sx;
	struct bstree_node *dx;
	int key;
	void *valptr;
	size_t val_lenght;
};

struct bstree {
	struct bstree_node *root;	/* Tree root */
	size_t 		 		size;	/* Num of elements in the tree */
};

typedef void (* print_func)(const void *p);

/*
 * Core functions
 */

void bstree_init(struct bstree *tree);

void bstree_free(struct bstree *tree);

/*
 * Modification and utilities
 */
int bstree_insert(struct bstree *bstree, void *ptrval,size_t val_lenght);

void bstree_delete(struct bstree *bstree, void *ptrval,size_t val_lenght);

int bstree_replace(struct bstree *bstree, void *new_ptrval,size_t new_lenght, void * old_ptrval,size_t old_lenght);

void bstree_print(struct bstree *bstree, print_func printer);
/*
 * Testing functions
 */

int bstree_init_rand(struct bstree *tree, size_t lenght);

int bstree_init_incr(struct bstree *bstree, size_t elements);


#endif /* BSTREE_H_ */
