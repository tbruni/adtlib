#ifndef TEST_H_
#define TEST_H_

#include <stdio.h>

#define TEST_VERBOSE	0

#define TEST(func)  do { \
	printf("Run: %s\n", #func); \
	(func)(); \
} while (0)

#endif
