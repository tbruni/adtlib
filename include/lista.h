/*
 * lista.h
 *
 *  Created on: 26 ott 2018
 *      Author: tbruni
 */

#ifndef LISTA_H_
#define LISTA_H_

typedef void (* print_func)(const void *p);

struct elemento{
	struct elemento *next;
	struct elemento *prec;
	void *ptrval;
	size_t size;
	print_func printer;
};

struct list{
	struct elemento *head;
	struct elemento *tail;
	size_t num_el;
};



void list_init(struct list *p);
int list_insert(struct list *punta_list,void *new_el,size_t size_el,print_func printer);
void list_release(struct list *p);
size_t list_lenght(struct list *p);
int list_delete(struct list *nodo, void *val_elm,size_t size_elm);
void list_print(struct list *elem);
void list_print_reverse(struct list *elem);




#endif /* LISTA_H_ */
