/*
 ============================================================================
 Name        : lista.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lista.h"


/*
void test_lista(int val)
{
	struct elemento *head;
	struct elemento *tail;
	struct elemento *tmp;
	int i;
	head=NULL;
	for(i=0;i<val;i++)
	{
			tmp=malloc(sizeof(struct elemento));
			tmp->next = head;
			tmp->prec = NULL;
			tmp->valore = i*5;

			if (head != NULL)
				head->prec = tmp;
			else
				tail = tmp;
			head=tmp;
	}
#if 0
	curr=head;
	while(curr->next!=NULL){
		tmp=curr->next;
		tmp->prec=curr;
		curr=tmp;
		conta++;
	}
#endif
	stampa(head);
	stampa_reverse(tail);
	while(curr!=NULL)
	{
		if(curr->valore==ric)
		{
			conta++;
		}
		curr=curr->next;
	}
	printf("il valore e' stato trovato %d volte ",conta);

	while(head!=NULL)
	{
		tmp=head;

		head=head->next;
		free(tmp);
	}
	free(head);
}
*/

void list_init(struct list *p)
{
	p->head=NULL;
	p->tail=NULL;
	p->num_el=0;
}

static int element_init(struct elemento *elem,
						 struct elemento *next, struct elemento *prec,
						 void *ptrval, size_t size,
						 print_func printer)
{
	elem->ptrval = malloc(size);
	if (elem->ptrval == NULL)
		return -1;
	memcpy(elem->ptrval, ptrval, size);
	elem->size = size;
	elem->next = next;
	elem->prec = prec;
	elem->printer = printer;
	if(next != NULL)
		next->prec = elem;
	return 0;
}

static struct elemento *element_create(struct elemento *next, struct elemento *prec,
						 	 	 	   void *ptrval, size_t size, print_func printer)
{
	struct elemento *elem;

	elem=malloc(sizeof(struct elemento));
	if(elem==NULL)
		return NULL;

	if (element_init(elem, next, prec, ptrval, size, printer) < 0) {
		free(elem);
		return NULL;
	}
	return elem;
}

int list_insert(struct list *list,void *el,size_t size_el,print_func printer)
{
	struct elemento *tmp;

	tmp = element_create(list->head, NULL, el, size_el, printer);
	if (tmp == NULL)
		return -1;

	list->head=tmp;
	if(list->tail == NULL)
		list->tail=tmp;
	list->num_el++;
	return 0;
}

int list_delete(struct list *list, void *val_elm,size_t size_elm)
{
	struct elemento *curr;
	int l;

#if 0
	l = (list->head->size < size_elm) ? list->head->size  : size_elm;
	if(memcmp(list->head->ptrval,val_elm,l) == 0 && size_elm == list->head->size) {
		p=list->head;
		list->head=list->head->next;
		list->head->prec=NULL;
		free(p);
		list->num_el--;
		return 0;
	}
#endif

	curr=list->head;
	while (curr != NULL) {
		l=(curr->size < size_elm) ? curr->size : size_elm;
		if(memcmp(curr->ptrval,val_elm,l) != 0 && curr->size != size_elm) {
			curr=curr->next;
		} else {
			break;
		}
	}
	if(curr==NULL)
		return -1;

	if(curr==list->head && list->head->next == NULL){
		list->head=NULL;
		list->tail=NULL;
		free(list->head);
		list->num_el--;
	}else{
		if (curr == list->head) {
			/* it is the head*/
				list->head=list->head->next;
				list->head->prec=NULL;
				free(curr);
		} else if (curr == list->tail) {
			/* it is the tail*/
				list->tail=list->tail->prec;
				list->tail->next=NULL;
				free(curr);
		} else {
			/* it is in the middle */
			curr->prec->next=curr->next;
			curr->next->prec=curr->prec;
			free(curr);
		}
		list->num_el--;
	}
	return 0;

#if 0
	if(memcmp(list->tail->ptrval,val_elm,l)==0 && list->tail->size == size_elm)	{
		tmp=list->tail;
		list->tail=list->tail->prec;
		list->tail->next=NULL;
		free(tmp);
	} else {
		p->next=curr->next;
		curr->next->prec=p;
		free(curr);
	}
	list->num_el--;
#endif
}


void list_release(struct list *list)
{
	struct elemento *tmp;

	while(list->head!=NULL) {
		tmp = list->head;
		list->head=list->head->next;
		free(tmp->ptrval);
		free(tmp);
	}
}


size_t list_lenght(struct list *p)
{
	return p->num_el;
}



static void print_pvt(struct elemento *curr, int reverse)
{
	if(reverse==0){
		printf("stampa normale:\n");
		while(curr!=NULL) {
			curr->printer(curr->ptrval);
			fflush(stdout);
			curr=curr->next;
		}
	} else{
		printf("stampa al contrario:\n");
		while(curr!=NULL){
			curr->printer(curr->ptrval);
			fflush(stdout);
			curr=curr->prec;
		}
	}
}

void list_print(struct list *elem)
{
	if(elem->head!=NULL)
		print_pvt(elem->head, 0);
	else
		printf("nessun elemento nella lista\n");
	fflush(stdout);
}



void list_print_reverse(struct list *list)
{
	if(list->tail!=NULL)
		print_pvt(list->tail, 1);
	else
		printf("nessun elemento nella lista\n");
	fflush(stdout);
}



#if 0
void list_print_reverse_old(struct list *list)
{
	int i=0;
	int *tmp;
	struct elemento *curr;
	int k=0;
	curr=list->head;
	while(curr!=NULL)
	{
		curr=curr->next;
		i++;
	}
	curr=list->head;
	tmp=malloc(i*sizeof(int));
	while(curr!=NULL) {
		tmp[k]=curr->valore;
		k++;
		curr=curr->next;
	}

	while(i>0)
	{
		i--;
		printf("%d\n",tmp[i]);

	}
}
#endif


