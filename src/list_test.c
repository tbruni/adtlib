/*
 * list_test.c
 *
 *  Created on: 5 nov 2018
 *      Author: tbruni
 */
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include "lista.h"
#include "test.h"

void print_int_list(int *p)
{
	printf("%d\n",*p);
}

void print_string_list(char *p)
{
	printf("%s\n",p);
}

void list_empty()
{
	struct list a;
	size_t len;

	/* Setup */
	list_init(&a);
	/* Test */
	len = list_lenght(&a);
	/* Check */
	assert(len == 0);
#if TEST_VERBOSE
	list_print(&a);
#endif
	/* Cleanup */
	list_release(&a);
}

void list_not_empty()
{
	struct list a;
	size_t len;

	/* Setup */
	list_init(&a);
	list_insert(&a,"ciao",5,(print_func) print_string_list);
	/* Test */
	len = list_lenght(&a);
	/* Check */
	assert(len == 1);
#if TEST_VERBOSE
	list_print(&a);
	list_print_reverse(&a);
#endif
	/* Cleanup */
	list_release(&a);
}

void list_multiple_insert()
{
	struct list a;
	size_t len;

	/* Setup */
	list_init(&a);
	list_insert(&a,"ciao",5,(print_func) print_string_list);
	list_insert(&a,"tommaso",8,(print_func) print_string_list);
	list_insert(&a,"bruni",6,(print_func) print_string_list);
	list_insert(&a,"hello",6,(print_func) print_string_list);
	/* Test */
	len = list_lenght(&a);
	/* Check */
	assert(len == 4);
#if TEST_VERBOSE
	list_print(&a);
	list_print_reverse(&a);
#endif
	/* Cleanup */
	list_release(&a);
}

void list_multiple_insert2()
{
	int i;
	struct list a;
	size_t len;

	/* Setup */
	list_init(&a);
	for(i=0;i<5;i++)
		list_insert(&a,&i,sizeof(int),(print_func) print_int_list);
	/* Test */
	len = list_lenght(&a);
	/* Check */
	assert(len == 5);
#if TEST_VERBOSE
	list_print(&a);
	list_print_reverse(&a);
#endif
	/* Cleanup */
	list_release(&a);
}

void list_multiple_insert3()
{
	struct list a;
	int g=10;
	size_t len;

	/* Setup */
	list_init(&a);
	list_insert(&a,&g,sizeof(int),(print_func) print_int_list);
	list_insert(&a,"ciao",5,(print_func) print_string_list);
	g=100;
	list_insert(&a,&g,sizeof(int),(print_func) print_int_list);
	list_insert(&a,"hello",6,(print_func) print_string_list);
	/* Test */
	len = list_lenght(&a);
	/* Check */
	assert(len == 4);
#if TEST_VERBOSE
	list_print(&a);
	list_print_reverse(&a);
#endif
	/* Cleanup */
	list_release(&a);
}

void list_del()
{
	struct list a;
	size_t len;
	int del;

	/* Setup */
	list_init(&a);
	list_insert(&a,"ciao",5,(print_func) print_string_list);
	list_insert(&a,"tommaso",8,(print_func) print_string_list);
	list_insert(&a,"bruni",6,(print_func) print_string_list);
	list_insert(&a,"hello",6,(print_func) print_string_list);
	/* Test */
	len=list_lenght(&a);
	/* Check */
	assert(len == 4);
	/* Test */
	del=list_delete(&a,"hello",6);
	len=list_lenght(&a);
	/* Check */
	assert(len == 3);
	assert(del == 0);
#if TEST_VERBOSE
	list_print(&a);
	list_print_reverse(&a);
#endif
	/* Cleanup */
	list_release(&a);
}

void insert_list_0e()
{
	struct list list;

	/* setup */
	list_init(&list);
	/* test */
	list_insert(&list,"ciao",5,(print_func) print_string_list);
	/* check */
	assert(list_lenght(&list) == 1);
	assert(list.head->size==5);
	assert(memcmp(list.head->ptrval,"ciao",5)==0);
	assert(list.head==list.tail);
	assert(list.head->prec==NULL);
	assert(list.head->next==NULL);
	/* cleanup */
	list_release(&list);
}

void insert_list_1e()
{
	struct list list;
	struct elemento *prev_head;

	/* setup */
	list_init(&list);
	list_insert(&list,"ciao",5,(print_func) print_string_list);
	prev_head=list.head;
	/* test */
	list_insert(&list,"hello",6,(print_func) print_string_list);
	/* check */
	assert(list_lenght(&list) == 2);
	assert(list.head->prec==NULL);
	assert(list.tail->next==NULL);
	assert(list.head->size==6);
	assert(memcmp(list.head->ptrval,"hello",6)==0);
	assert(list.head->next == prev_head);
	assert(list.tail->prec == list.head);
	assert(list.tail == prev_head);
	/* cleanup */
	list_release(&list);
}

void insert_list_2e()
{
	struct list list;
	struct elemento *prev_head;

	/* setup */
	list_init(&list);
	list_insert(&list,"ciao",5,(print_func) print_string_list);
	list_insert(&list,"hello",6,(print_func) print_string_list);
	prev_head=list.head;
	/* test */
	list_insert(&list,"tommaso",8,(print_func) print_string_list);
	/* check */
	assert(list_lenght(&list) == 3);
	assert(prev_head==list.head->next);
	assert(list.tail->next==NULL);
	assert(list.tail->prec==list.head->next);
	assert(list.head->next->prec==list.head);
	assert(list.tail->prec->next==list.tail);
	assert(list.head->prec==NULL);
	assert(list.tail->prec->prec==list.head);
	/* cleanup */
	list_release(&list);
}


void del_list_0e()
{
	struct list list;
	int len;
	int res;
	/* setup */
	list_init(&list);
	/* test */
	res=list_delete(&list,"ciao",5);
	len=list_lenght(&list);
	/* check */
	assert(res==-1);
	assert(len == 0);
	assert(list.head==NULL);
	assert(list.tail==NULL);
	/* Cleanup */
	list_release(&list);
}

void del_list_1e()
{
	struct list list;
	int len;
	int res;
	/* setup */
	list_init(&list);
	list_insert(&list,"ciao",5,(print_func) print_string_list);
	/* test */
	len=list_lenght(&list);
	/* check */
	assert(len==1);
	/* test */
	res=list_delete(&list,"ciao",5);
	len=list_lenght(&list);
	/* check */
	assert(len==0);
	assert(res==0);
	assert(list.head==NULL);
	assert(list.tail==NULL);
	/* Cleanup */
	list_release(&list);
}

void list_test()
{
	TEST(list_empty);
	TEST(list_not_empty);
	TEST(list_multiple_insert);
	TEST(list_multiple_insert2);
	TEST(list_multiple_insert3);
	TEST(list_del);
	TEST(insert_list_0e);
	TEST(insert_list_1e);
	TEST(insert_list_2e);
	TEST(del_list_0e);
	TEST(del_list_1e);
}
