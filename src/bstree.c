/*
 ============================================================================
 Name        : albero.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "bstree.h"

#if 1
static void node_print(struct bstree_node *bstree_node, print_func printer)
{
	if(bstree_node!=NULL)
	{
		node_print(bstree_node->sx,printer);
		printer(bstree_node->valptr);
		node_print(bstree_node->dx,printer);
	}
}
#endif


#if 1
void bstree_print(struct bstree *bstree,print_func printer)
{
	node_print(bstree->root,printer);
}
#endif

int hashbuf(void *ptr,size_t size)
{
	int h=0;
	int i;
	char *p;
	p=(char *)ptr;

	for(i=0;i<size;i++)
		h+=p[i] * i;

	h = h%8;
	return h;
}

#if 1
static void del_node(struct bstree_node *curr, struct bstree_node *parent,int is_left)
{
	struct bstree_node *tmp;

	tmp=curr->dx;

	if(is_left!=0)
	{
		parent->dx=curr->dx;

		while(tmp!=NULL)
		{
			parent=tmp;
			tmp=tmp->sx;
		}
		parent->sx=curr->sx;
	}
	else
	{
		parent->sx=curr->dx;

		while(tmp!=NULL)
		{
			parent=tmp;
			tmp=tmp->sx;
		}
		parent->sx=curr->sx;

	}
}
#endif


#if 1
void bstree_delete(struct bstree *bstree, void *ptrval,size_t val_lenght)
{
	struct bstree_node *curr;
	struct bstree_node *p;
	int key_del;


	key_del = hashbuf (ptrval, val_lenght);
	curr = bstree->root;
	p = NULL;


	int l = (val_lenght < curr->val_lenght) ? val_lenght : curr->val_lenght;
	if(key_del == curr->key && memcmp(ptrval,curr->valptr,l)==0 && val_lenght == curr->val_lenght)
	{
		if(curr->dx != NULL)
		{
			curr = curr->dx;
			while(curr != NULL)
			{
				p = curr;
				curr = curr->sx;
			}
			p->sx = bstree->root->sx;
			curr = bstree->root;
			bstree->root = bstree->root->dx;
			free(curr);
		}
		else /* if(curr->sx != NULL) */
		{
			curr = bstree->root;
			bstree->root = bstree->root->sx;
			free(curr);
		}
		bstree->size--;
	}
	else
	{
		while(curr != NULL/* && key_del != curr->key && memcmp(ptrval,curr->valptr,l)!=0 && val_lenght != curr->val_lenght*/)
		{
			if(key_del>curr->key)
			{
				p=curr;
				curr=curr->dx;
			}
			else if(key_del<curr->key)
			{
				p=curr;
				curr=curr->sx;
			}
			else
				{
					if(memcmp(ptrval,curr->valptr,l)<0)
					{
						p=curr;
						curr=curr->sx;
					}
					else if(memcmp(ptrval,curr->valptr,l)>0)
					{
						p=curr;
						curr=curr->dx;
					}
					else
					{
						if (val_lenght < curr->val_lenght) {
							p=curr;
							curr=curr->sx;
						} else if (val_lenght > curr->val_lenght) {
							p=curr;
							curr=curr->dx;
						}
						else {
							break;
						}
					}
				}
		}
		if(curr==NULL)
		{
			printf("nodo non presente nell'albero");
		}
		else
		{
			if(p->dx==curr)
			{
				if (curr->dx == NULL)
					p->dx = curr->sx;
				else
					del_node(curr,p,1);
			}
			else /* if(p->sx==curr) */
			{
				if (curr->dx == NULL)
					p->sx = curr->sx;
				else
					del_node(curr,p,0);
			}
			free(curr);
			bstree->size--;
		}
	}
}
#endif

/* Inserts a new node on the bstree.
 * Returns:
 *   on success: 0
 *   on failure: -1
 */
int bstree_insert(struct bstree *bstree, void *ptrval, size_t val_lenght)
{

	int newkey=hashbuf(ptrval,val_lenght);


	struct bstree_node *parent = NULL;
	struct bstree_node *curr = bstree->root;
	struct bstree_node *newnode=NULL;
	while(curr!=NULL)
	{
		if(newkey>curr->key)
		{
			parent=curr;
			curr=curr->dx;
		}
		else if(newkey<curr->key)
		{
			parent=curr;
			curr=curr->sx;
		}
		else
		{
			int l = (val_lenght < curr->val_lenght) ? val_lenght : curr->val_lenght;
			if(memcmp(ptrval,curr->valptr,l)<0)
			{
				parent=curr;
				curr=curr->sx;
			}
			else if(memcmp(ptrval,curr->valptr,l)>0)
			{
				parent=curr;
				curr=curr->dx;
			}
			else
			{
				if (val_lenght < curr->val_lenght) {
					parent=curr;
					curr=curr->sx;
				} else if (val_lenght > curr->val_lenght) {
					parent=curr;
					curr=curr->dx;
				} else {
					break;
				}
			}
		}
	}

	if(curr==NULL)
	{
		newnode=malloc(sizeof(struct bstree_node));

		if (newnode == NULL)
			return -1;

		newnode->valptr=malloc(val_lenght);

		newnode->key=newkey;
		newnode->val_lenght=val_lenght;
		memcpy(newnode->valptr,ptrval,val_lenght);
		newnode->dx=NULL;
		newnode->sx=NULL;

		if(parent!=NULL)
		{
			if(newnode->key>parent->key){
				parent->dx=newnode;
			}
			else if(newnode->key<parent->key){
				parent->sx=newnode;
			}
			else{
				if(memcmp(newnode->valptr,parent->valptr,val_lenght)<0)
				{
					parent->sx=newnode;
				}
				else
				{
					parent->dx=newnode;
				}
			}
		}
		else
		{
			bstree->root = newnode;
		}
		bstree->size++;
	}
	return 0;
}



#if 1
int bstree_replace(struct bstree *bstree, void *new_ptrval,size_t new_lenght, void * old_ptrval,size_t old_lenght)
{
	int res = 0;
	int old_key = hashbuf(old_ptrval,old_lenght);
	int new_key = hashbuf(new_ptrval,new_lenght);

	int l = (new_lenght < old_lenght) ? new_lenght : old_lenght;

	if(new_key == old_key && memcmp(new_ptrval,old_ptrval,l) ==0 && new_lenght == old_lenght)
	{
		printf("nodo da aggiornare uguale a quello gi� presente\n");
		res=-1;
	}
	else
	{
		bstree_delete(bstree, old_ptrval,old_lenght);
		res = bstree_insert(bstree, new_ptrval,new_lenght);

	}
	return res;
}
#endif

void bstree_init(struct bstree *bstree)
{
	bstree->root = NULL;
	bstree->size = 0;
}



static void node_free(struct bstree_node *bstree_node)
{
	if (bstree_node == NULL)
		return;

	node_free(bstree_node->sx);
	node_free(bstree_node->dx);

	free(bstree_node->valptr);
	free(bstree_node);
}

void bstree_free(struct bstree *bstree)
{

	node_free(bstree->root);
}

#if 0
int bstree_init_rand(struct bstree *bstree, size_t lenght)
{
	int i;
	int val;
	int res = 0;

	bstree_init(bstree);

	for(i=0;i<lenght;i++)
	{
		val=rand()%12;
		res = bstree_insert(bstree, val);
		if (res < 0)
		{
			printf("Insertion failure\n");
			break;
		}
	}
	printf("elements:  %d\n", bstree->size);
	return res;
}

int bstree_init_incr(struct bstree *bstree, size_t elements)
{
	int i;

	bstree_init(bstree);
	for (i = elements; i > 0; i--) {
		if (bstree_insert(bstree, i) < 0)
			return -1;
	}
	return 0;
}
#endif


