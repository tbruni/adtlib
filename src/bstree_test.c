/*
 * bstree_test.c
 *
 *  Created on: 30 ott 2018
 *      Author: tbruni
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "bstree.h"
#include "test.h"

#define TYPE_INT	1
#define TYPE_STRING	2
#define TYPE_FLOAT	3

#define ELEM_SIZE_INT	(sizeof(struct elem) + sizeof(int) - 1)
#define ELEM_SIZE_FLOAT	(sizeof(struct elem) + sizeof(float) - 1)
#define ELEM_SIZE_STRING(slen)	(sizeof(struct elem) + (slen) - 1)


struct elem {
	char 	type;
	char 	buf[1];
};


void print_int(int *i)
{
	printf("%d\n", *i);
}

void print_float(float *f)
{
	printf("%f\n", *f);
}

void print_string(char *s)
{
	printf("%s\n", s);
}

void print_elem(struct elem *e)
{
	switch (e->type) {
	case TYPE_INT:
		print_int((int *)e->buf);
		break;
	case TYPE_STRING:
		print_string(e->buf);
		break;
	case TYPE_FLOAT:
		print_float((float *)e->buf);
		break;
	}
}

void bstree_test_elem(void)
{
	struct elem *e;
	struct bstree tree;

	bstree_init(&tree);

	e = (struct elem *)malloc(ELEM_SIZE_INT);
	e->type = TYPE_INT;
	(*(int *)e->buf) = 3;
	bstree_insert(&tree, e, ELEM_SIZE_INT);
	free(e);

	e = (struct elem *)malloc(ELEM_SIZE_FLOAT);
	e->type = TYPE_FLOAT;
	((float *)e->buf)[0] = 1.1234;
	bstree_insert(&tree, e, ELEM_SIZE_FLOAT);

	e = (struct elem *)malloc(ELEM_SIZE_STRING(6));
	e->type = TYPE_STRING;
	strcpy(e->buf, "Hello");
	bstree_insert(&tree, e, ELEM_SIZE_STRING(6));

	bstree_print(&tree, (print_func) print_elem);

	bstree_free(&tree);
}


void bstree_test_int(void)
{
	struct bstree tree;
	int d, i;

	bstree_init(&tree);
	for (i = 0; i < 10; i++) {
		d = rand();
		bstree_insert(&tree, &d, sizeof(int));
	}
	bstree_print(&tree,(print_func) print_int);
	bstree_free(&tree);

}


void bstree_test_string(void)
{
	struct bstree t1;
	//int a = 10;
	bstree_init(&t1);


	bstree_insert(&t1,"tommaso",8);
	bstree_insert(&t1,"franco",7);
	bstree_insert(&t1,"mmaoost",8);

//	bstree_insert(&t1,&a,sizeof(int));


	printf("pre-update\n");
	bstree_print(&t1,(print_func) print_string);
	fflush(stdout);

/*
	printf("after del\n");
	bstree_delete(&t1,"mmaoost",8);
	bstree_print(&t1,(print_func) print_string);

*/

	printf("after-update\n");
	bstree_replace(&t1,"roberto",8,"tommaso",8);
	bstree_print(&t1,(print_func) print_string);
	fflush(stdout);

	bstree_free(&t1);
	/*
	bstree_init_insert(&t1, "Tommaso", 8);
	bstree_init_insert(&t1, "Bruni", 6);
	bstree_print(&1, print_string);

	printf("albero iniziale\n");
*/

#if 0
	bstree_init_rand(&t1, 5);
	bstree_print(&t1);
	fflush(stdout);

	bstree_init_incr(&t2, 5);
	bstree_print(&t2);
	fflush(stdout);



	printf("albero con bstree_node cancellato\n");
	bstree_delete(&t1, 10);
	bstree_print(&t1);
	fflush(stdout);

	printf("albero con bstree_node aggiornato\n");
	bstree_replace(&t1,16,11);
	bstree_print(&t1);
	fflush(stdout);
	bstree_free(&t1);
#endif
}


void bstree_test(void)
{
	bstree_test_string();
	bstree_test_int();
	bstree_test_elem();
}
